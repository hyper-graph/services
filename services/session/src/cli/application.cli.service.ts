import { BaseApplicationCliService } from '../external';

export class ApplicationCliService extends BaseApplicationCliService {
	protected async getModule(): Promise<Class> {
		const { AppModule } = await import('../app');

		return AppModule;
	}
}
