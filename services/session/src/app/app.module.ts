import { BaseExecutionMode } from '@hg/common';
import { ConfigLoader } from '@hg/config-loader';
import { Module } from '@nestjs/common';
import {
	CommonGlobalModule,
	RabbitmqModule,
} from '../external';
import { RabbitmqConfig } from '../external/rabbitmq/rabbitmq.config';
import { ExampleService } from './example.service';

@Module({
	imports: [
		CommonGlobalModule,
		RabbitmqModule.forRootAsync({
			async useFactory(env: BaseExecutionMode): Promise<RabbitmqConfig> {
				return new ConfigLoader({
					configDir: 'config',
					configName: 'rabbitmq',
					overrideEnv: env,
				}).load(RabbitmqConfig);
			},
			inject: [BaseExecutionMode],
		}),
	],
	providers: [
		ExampleService,
		{
			provide: RabbitmqConfig,
			useValue: null,
		},
	],
})
export class AppModule {}
