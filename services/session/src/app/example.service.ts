import { BaseInjectable } from '@hg-nest/common';
import { Inject } from '@nestjs/common';

import { AmqpChannel } from '../external';

export class ExampleService extends BaseInjectable {
	@Inject()
	protected readonly channel: AmqpChannel;

	protected async handleApplicationBootstrap(): Promise<void> {
		await this.channel.onApplicationBootstrap();

		const queue = this.channel.getQueue('some_queue');
		await queue.assert();

		queue.getObservable().subscribe(message => {
			console.log(message.json);

			message.ack({ allUpTo: true });
		});

		// eslint-disable-next-line @typescript-eslint/no-misused-promises
		setInterval(async() => {
			await queue.send({ someData: 'sdfsdf' });
		}, 1000);
	}
}
