import { Module } from '@nestjs/common';
import { ApplicationCliService } from './cli';

@Module({ providers: [ApplicationCliService] })
export class MainModule {
}
