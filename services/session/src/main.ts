import '@hg/types';
import { CliApplication } from './external';
import { MainModule } from './main.module';

await CliApplication.run(MainModule);
