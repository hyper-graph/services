import { BaseException } from '@hg/exception';

export class AmqpConnectionBlockedException extends BaseException {
	public constructor(reason: string) {
		super(reason);
	}
}
