import { EnforceEnv } from '@hg/config-loader';
import {
	IsInt,
	IsPositive,
	IsString,
} from 'class-validator';

export class RabbitmqConfig {
	@IsString()
	public readonly protocol: string = 'amqps';

	@IsString()
	@EnforceEnv('HG_RABBITMQ_HOST')
	public readonly host: string;

	@IsInt()
	@IsPositive()
	@EnforceEnv('HG_RABBITMQ_PORT')
	public readonly port: number = 5672;

	@IsString()
	@EnforceEnv('HG_RABBITMQ_USER')
	public readonly user: string;

	@IsString()
	@EnforceEnv('HG_RABBITMQ_PASSWORD')
	public readonly password: string;

	@IsString()
	@EnforceEnv('HG_RABBITMQ_VHOST')
	public readonly vhost: string = '/';

	@IsString()
	@EnforceEnv('HG_RABBITMQ_CA_PATH')
	public readonly caPath: string;

	@IsString()
	@EnforceEnv('HG_RABBITMQ_CERT_PATH')
	public readonly certPath: string;

	@IsString()
	@EnforceEnv('HG_RABBITMQ_KEY_PATH')
	public readonly keyPath: string;
}
