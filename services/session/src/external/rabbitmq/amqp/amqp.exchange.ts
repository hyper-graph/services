import {
	AmqpBaseEntity,
	SendOptions,
} from './amqp.base.entity';

export type ExchangeDeleteOptions = {
	ifUnused?: boolean;
};

export type ExchangeAssertOptions = {
	durable?: boolean;
	internal?: boolean;
	autoDelete?: boolean;
	alternateExchange?: string;
};

export type ExchangeAssertResult = {
	exchangeName: string;
};

export enum ExchangeType {
	DIRECT = 'direct',
	TOPIC = 'topic',
	HEADERS = 'headers',
	FANOUT = 'fanout',
	MATCH = 'match',
}

export class AmqpExchange extends AmqpBaseEntity {
	public async send(routingKey: string, content: unknown, options: Partial<SendOptions> = {}): Promise<void> {
		await this.wrapper.publish(this.name, routingKey, content, {
			timestamp: Date.now(),
			...options,
		});
	}

	public async assert(type: ExchangeType | string, options: ExchangeAssertOptions = {}): Promise<ExchangeAssertResult> {
		const { exchange } = await this.wrapper.assertExchange(this.name, type, options);

		this.name = exchange;

		return { exchangeName: exchange };
	}

	public async delete(options: Partial<ExchangeDeleteOptions> = {}): Promise<void> {
		await this.wrapper['_channel'].deleteExchange(this.name, options);
	}

	protected async rawCheck(): Promise<void> {
		await this.wrapper['_channel'].checkExchange(this.name);
	}

	protected async rawBind(exchange: string, routingKey: string): Promise<void> {
		await this.wrapper['_channel'].bindExchange(this.name, exchange, routingKey);
	}

	protected async rawUnbind(exchange: string, routingKey: string): Promise<void> {
		await this.wrapper['_channel'].unbindExchange(this.name, exchange, routingKey);
	}
}
