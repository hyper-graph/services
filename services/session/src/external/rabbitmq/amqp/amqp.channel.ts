import { BaseInjectable } from '@hg-nest/common';
import { Logger } from '@hg-nest/logger';
import { Inject } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';

import type { ChannelWrapper } from 'amqp-connection-manager';

import { AmqpConnection } from './amqp.connection';
import { AmqpExchange } from './amqp.exchange';
import { AmqpQueue } from './amqp.queue';

export type NackAllOptions = {
	requeue?: boolean;
};

export class AmqpChannel extends BaseInjectable {
	@Inject()
	protected readonly logger: Logger;

	@Inject()
	protected readonly moduleRef: ModuleRef;

	protected channel: ChannelWrapper;
	private name: string;

	public constructor(connection?: AmqpConnection, name?: string) {
		super();

		this.name = name ?? 'unknown';

		if (connection instanceof AmqpConnection) {
			this.initFromConnection(connection);
		}
	}

	public async connect(): Promise<void> {
		await this.channel.waitForConnect();
	}

	public async close(): Promise<void> {
		await this.channel.close();
	}

	public ackAll(): void {
		this.channel.ackAll();
	}

	public nackAll({ requeue = false }: NackAllOptions = {}): void {
		this.channel.nackAll(requeue);
	}

	public getQueue(name: string): AmqpQueue {
		return new AmqpQueue(this, name);
	}

	public getExchange(name: string): AmqpExchange {
		return new AmqpExchange(this, name);
	}

	protected async handleApplicationBootstrap(): Promise<void> {
		const connection = this.moduleRef.get(AmqpConnection);

		await connection.onApplicationBootstrap();

		this.name = 'default';
		this.initFromConnection(connection);
		await this.channel.waitForConnect();
	}

	private initFromConnection(connection: AmqpConnection): void {
		this.channel = connection['connection'].createChannel({
			name: this.name,
			json: true,
		});

		this.channel.on('connect', () => {
			this.logger.info(`Channel with name "${this.name}" connected`);
		});
		this.channel.on('close', () => {
			this.logger.info(`Channel with name "${this.name}" closed`);
		});
		this.channel.on('error', (error: Error) => {
			this.logger.error('Error occurred:', error);
		});
	}
}
