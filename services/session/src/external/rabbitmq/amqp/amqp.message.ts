import type { Message } from 'amqplib';
import type { AmqpChannel } from './amqp.channel';

export type AckOptions = {
	allUpTo?: boolean;
};

export type NackOptions = {
	allUpTo?: boolean;
	requeue?: boolean;
};

export class AmqpMessage {
	protected readonly message: Message;
	protected readonly channel: AmqpChannel;

	public constructor(rawMsg: Message, channel: AmqpChannel) {
		this.message = rawMsg;
		this.channel = channel;
	}

	public get json(): unknown {
		return JSON.parse(this.string);
	}

	public get buffer(): Buffer {
		return this.message.content;
	}

	public get string(): string {
		return this.buffer.toString('utf-8');
	}

	public ack(options: AckOptions = {}): void {
		const { allUpTo = false } = options;

		this.channel['channel'].ack(this.message, allUpTo);
	}

	public nack(options: NackOptions = {}): void {
		const { allUpTo = false, requeue = false } = options;

		this.channel['channel'].nack(this.message, allUpTo, requeue);
	}
}
