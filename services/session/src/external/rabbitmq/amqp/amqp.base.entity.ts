import type { ChannelWrapper } from 'amqp-connection-manager';
import type { AmqpChannel } from './amqp.channel';

export type BaseDeleteOptions = {
	ifUnused?: boolean;
};

export type AssertResult = {
	queueName: string;
	messageCount: number;
	consumerCount: number;
};

export type SendOptions = {
	expiration?: number | string;
	userId?: string;
	CC?: string[] | string;
	BCC?: string[] | string;
	mandatory?: boolean;
	persistent?: boolean;

	contentType?: string;
	contentEncoding?: string;
	headers?: Record<string, unknown>;
	priority?: number;
	correlationId?: string;
	replyTo?: string;
	messageId?: string;
	timestamp?: number;
	type?: string;
	appId?: string;
	timeout?: number;
};

export abstract class AmqpBaseEntity {
	protected name: string;
	protected readonly chanel: AmqpChannel;

	public constructor(channel: AmqpChannel, name: string) {
		this.chanel = channel;
		this.name = name;
	}

	public async bind(exchange: string, routingKey: string): Promise<void> {
		await this.rawBind(exchange, routingKey);
	}

	public async unbind(exchange: string, routingKey: string): Promise<void> {
		await this.wrapper['_channel'].unbindQueue(this.name, exchange, routingKey);
	}

	public async exist(): Promise<boolean> {
		try {
			await this.wrapper.checkQueue(this.name);

			return true;
			// eslint-disable-next-line @typescript-eslint/no-implicit-any-catch
		} catch (error: any) {
			const code = error.code as number;

			if (code !== 404) {
				throw error;
			}

			return false;
		}
	}

	protected abstract rawCheck(): Promise<void>;
	protected abstract rawUnbind(exchange: string, routingKey: string): Promise<void>;
	protected abstract rawBind(exchange: string, routingKey: string): Promise<void>;

	protected get wrapper(): ChannelWrapper {
		return this.chanel['channel'];
	}
}
