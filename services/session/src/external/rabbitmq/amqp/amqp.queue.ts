import { Observable } from 'rxjs';
import type amqplib from 'amqplib';
import { v4 as uuid } from 'uuid';

import {
	AmqpBaseEntity,
	BaseDeleteOptions,
	SendOptions,
} from './amqp.base.entity';
import type { AmqpChannel } from './amqp.channel';
import { AmqpMessage } from './amqp.message';

export type QueueDeleteOptions = BaseDeleteOptions & {
	ifEmpty?: boolean;
};

export type QueueAssertOptions = {
	exclusive?: boolean;
	durable?: boolean;
	autoDelete?: boolean;
	arguments?: any;
	messageTtl?: number;
	expires?: number;
	deadLetterExchange?: string;
	deadLetterRoutingKey?: string;
	maxLength?: number;
	maxPriority?: number;
};

export type QueueAssertResult = {
	queueName: string;
	messageCount: number;
	consumerCount: number;
};

export class AmqpQueue extends AmqpBaseEntity {
	public getObservable<Message extends AmqpMessage = AmqpMessage>(
		messageCtor: Class<Message, [amqplib.Message, AmqpChannel]> = AmqpMessage as any,
	): Observable<Message> {
		return new Observable<Message>(subscriber => {
			const consumerTag = uuid();
			const handler = (msg: amqplib.Message): void => subscriber.next(new messageCtor(msg, this.chanel));
			const options = { consumerTag };

			this.wrapper.consume(this.name, handler, options)
				.catch(error => subscriber.error(error));

			return (): void => this.cancelConsumer(consumerTag);
		});
	}

	public async send(content: unknown, options: Partial<SendOptions> = {}): Promise<void> {
		await this.wrapper.sendToQueue(this.name, content, {
			timestamp: Date.now(),
			...options,
		});
	}

	public async assert(options: QueueAssertOptions = {}): Promise<QueueAssertResult> {
		const { queue, messageCount, consumerCount } = await this.wrapper.assertQueue(this.name, options);

		this.name = queue;

		return { messageCount, consumerCount, queueName: queue };
	}

	public async delete(options: Partial<QueueDeleteOptions> = {}): Promise<number> {
		const { messageCount } = await this.wrapper.deleteQueue(this.name, options);

		return messageCount;
	}

	public async purge(): Promise<number> {
		const { messageCount } = await this.wrapper.purgeQueue(this.name);

		return messageCount;
	}

	protected async rawCheck(): Promise<void> {
		await this.wrapper.checkQueue(this.name);
	}

	protected async rawBind(exchange: string, routingKey: string): Promise<void> {
		await this.wrapper.bindQueue(this.name, exchange, routingKey);
	}

	protected async rawUnbind(exchange: string, routingKey: string): Promise<void> {
		await this.wrapper['_channel'].unbindQueue(this.name, exchange, routingKey);
	}

	private cancelConsumer(consumerTag: string): void {
		const consumers = this.wrapper['_consumers'] as any[];
		const consumerIndex = consumers.findIndex(consumer => consumer.consumerTag === consumerTag);

		if (consumerIndex !== -1) {
			consumers.splice(consumerIndex, 1);
		}
	}
}
