import { BaseInjectable } from '@hg-nest/common';
import { Logger } from '@hg-nest/logger';
import { Inject } from '@nestjs/common';
import { readFile } from 'fs/promises';
import {
	AmqpConnectionManager,
	connect,
} from 'amqp-connection-manager';
import type amqp from 'amqplib';

import { RabbitmqConfig } from '../rabbitmq.config';

export class AmqpConnection extends BaseInjectable {
	@Inject()
	protected readonly config: RabbitmqConfig;

	@Inject()
	protected readonly logger: Logger;

	protected connection: AmqpConnectionManager;

	protected async handleApplicationBootstrap(): Promise<void> {
		await this.connect();
	}

	private async connect(): Promise<void> {
		const { host, port, password, user, vhost, protocol, caPath, certPath, keyPath } = this.config;
		const certPaths = [caPath, certPath, keyPath];
		const [ca, cert, key] = await Promise.all(certPaths.map(async path => readFile(path)));

		this.connection = connect({
			protocol,
			port,
			password,
			vhost,
			username: user,
			hostname: host,
		}, {
			reconnectTimeInSeconds: 5,
			connectionOptions: { ca, cert, key },
		});


		this.connection.on('disconnect', ({ err: error }) => {
			const code = (error as any).code as number;

			if (code !== 404) {
				this.logger.alert(error);
			}
		});
		this.connection.on('connect', ({ url }) => {
			const connectionString = typeof url === 'string'
				? url
				: AmqpConnection.mapOptionsToUrl(url);

			this.logger.info('Successfully connected to client with url:', connectionString);
		});

		await this.connection.connect();
	}

	private static mapOptionsToUrl(options: amqp.Options.Connect): string {
		const {
			username,
			password,
			protocol = 'amqp',
			hostname = 'localhost',
			port = 5672,
			vhost = '/',
		} = options;
		const protoString = `${protocol}://`;
		const loginString = username ? `${username}${password ? `:${'*'.repeat(password.length)}` : ''}` : '';
		const hostString = `${hostname}:${port}${vhost}`;

		return `${protoString}${loginString}${loginString ? '@' : ''}${hostString}`;
	}
}
