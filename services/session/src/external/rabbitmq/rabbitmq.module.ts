import {
	DynamicModule,
	Global,
	Module,
} from '@nestjs/common';

import { AmqpChannel, AmqpConnection } from './amqp';

import { RabbitmqConfig } from './rabbitmq.config';

type ForRootAsyncFactory = {
	useFactory(...args: unknown[]): Promise<RabbitmqConfig> | RabbitmqConfig;
	inject?: unknown[];
};
type ForRootAsyncClass = {
	useClass: Class<RabbitmqConfig>;
};
type ForRootAsyncValue = {
	useValue: RabbitmqConfig;
};

export type ForRootAsyncOptions = ForRootAsyncClass | ForRootAsyncFactory | ForRootAsyncValue;

@Global()
@Module({
	providers: [
		AmqpConnection,
		AmqpChannel,
	],
	exports: [AmqpConnection, AmqpChannel],
})
export class RabbitmqModule {
	public static forRootAsync(options: ForRootAsyncOptions): DynamicModule {
		return {
			module: RabbitmqModule,
			providers: [
				{
					provide: RabbitmqConfig,
					...options as any,
				},
			],
		};
	}
}
