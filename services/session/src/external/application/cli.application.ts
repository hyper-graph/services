import { NestFactory } from '@nestjs/core';
import { CommandService } from 'nestjs-command/dist/index.js';
import { InternalMainModule } from './internal-main.module';

export class CliApplication {
	public static async run(mainModule: Class): Promise<void> {
		const module = InternalMainModule.register(mainModule);
		const context = await NestFactory.createApplicationContext(module, {
			abortOnError: false,
			bufferLogs: true,
		});
		await context.init();

		const commandService = await context.resolve(CommandService);

		await commandService.exec();
	}
}
