import { LoggerModule } from '@hg-nest/logger';
import { AppPathModule } from '@hg-nest/common';
import {
	BaseExecutionMode,
	HgExecutionMode,
} from '@hg/common';
import {
	Global,
	Module,
} from '@nestjs/common';

@Global()
@Module({
	imports: [LoggerModule, AppPathModule],
	providers: [
		{
			provide: BaseExecutionMode,
			useClass: HgExecutionMode,
		},
	],
	exports: [BaseExecutionMode],
})
export class CommonGlobalModule {
}
