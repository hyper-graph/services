import { Logger } from '@hg-nest/logger';
import { Inject } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Command } from 'nestjs-command/dist/index.js';

export abstract class BaseApplicationCliService {
	@Inject()
	protected readonly logger: Logger;

	@Command({ command: 'application:run' })
	public async applicationRun(): Promise<void> {
		return this.run();
	}

	protected async run(): Promise<void> {
		// Split applications logs
		this.logger.info();
		this.logger.info();
		this.logger.info();

		const context = await NestFactory.createApplicationContext(this.getModule(), {
			bufferLogs: true,
			abortOnError: false,
		});

		await context.init();
	}

	protected abstract getModule(): Promise<Class>;
}
