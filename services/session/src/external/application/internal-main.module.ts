import {
	DynamicModule,
	Module,
} from '@nestjs/common';
import { CommandModule } from 'nestjs-command/dist/index.js';
import { CommonGlobalModule } from './common.global.module';

@Module({ imports: [CommandModule, CommonGlobalModule] })
export class InternalMainModule {
	public static register(mainModule: Class): DynamicModule {
		return {
			module: InternalMainModule,
			imports: [mainModule],
		};
	}
}
